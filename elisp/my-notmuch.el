(use-package sendmail
  :defer t
  :commands (mail-mode mail-text)
  :defines (send-mail-function)
  :config

  (setq send-mail-function 'sendmail-send-it
		sendmail-program "/usr/bin/msmtp"
		mail-specify-envelope-from t)
)
(use-package message
  :commands (message-mode message-cite-original-without-signature)
  :config

  ;; When composing a mail, start the auto-fill-mode.
  (add-hook 'message-mode-hook 'turn-on-auto-fill)
  ;; (add-hook 'message-setup-hook 'bbdb-define-all-aliases)

  ;; Generate the mail headers before you edit your message.
  (setq message-generate-headers-first t)

  ;; The message buffer will be killed after sending a message.
  (setq message-kill-buffer-on-exit t)

  ;; When I reply, I don't want to have me in To or Cc
  (setq message-dont-reply-to-names (concat "\\("
											user-mail-address
											;; Nor the Debian BTS
											;; "\\|^submit@bugs.debian\\.org$"
											"\\)"))

  ;; based on http://mbork.pl/2016-02-06_An_attachment_reminder_in_mu4e
  (defun my-message-attachment-present-p ()
	"Return t if an attachment is found in the current message."
	(save-excursion
	  (save-restriction
		(widen)
		(goto-char (point-min))
		(when (search-forward "<#part" nil t) t))))

  (defvar my-message-attachment-intent-re
	(regexp-opt '("I attach"
				  "I have attached"
				  "I've attached"
				  "I have included"
				  "I've included"
				  "see the attached"
				  "see the attachment"
				  "attached file"))
	"A regex which - if found in the message, and if there is no
attachment - should launch the no-attachment warning.")

  (defvar my-message-attachment-reminder
	"Are you sure you want to send this message without any attachment? "
	"The default question asked when trying to send a message
containing `my-message-attachment-intent-re' without an
actual attachment.")

  (defun my-message-warn-if-no-attachments ()
	"Ask the user if s?he wants to send the message even though
there are no attachments."
	(when (and (save-excursion
				 (save-restriction
				   (widen)
				   (goto-char (point-min))
				   (re-search-forward my-message-attachment-intent-re nil t)))
			   (not (my-message-attachment-present-p)))
	  (unless (y-or-n-p my-message-attachment-reminder)
		(keyboard-quit))))

  (add-hook 'message-send-hook #'my-message-warn-if-no-attachments)
)
(use-package mm-decode
  :defer t
  :config
  ;; Displaying zip/tar inline is a really, really stupid default!
  (setq mm-inlined-types
		(cl-remove-if (apply-partially #'string-match-p "\\(x-g?tar\\|zip\\)")
					  mm-inlined-types))
  )
(defun my-notmuch-hello ()
  (interactive)
  (notmuch-hello))
(use-package notmuch
  :straight t
  :commands (notmuch-hello)
  :init
  (setq notmuch-command "~/bin/notmuch")

  :config
  ;; Change flagged lines to be dark blue in the background, not blue
  ;; in the foreground. This harmonizes much more with my dark theme.
  (add-to-list 'notmuch-search-line-faces
  			   '("flagged" :background "dark blue"))

  ;; use our own notmuch binary
  (setq notmuch-search-result-format
		'(("date" . "%12s ")
		  ("count" . "%6s ")
		  ("authors" . "%-23s ")
		  ("subject" . "%s ")
		  ;; ("tags" . "(%s)")
		  ))

  ;; remove archiving commands
  (define-key notmuch-search-mode-map "a" nil)
)
(use-package notmuch-hello
  :commands (notmuch-hello-search notmuch-hello-delete-search-from-history
             notmuch-hello-insert-search notmuch-hello-nice-number
             notmuch-hello-widget-search)
)
;; This is GPLv2. If you still don't know the details, read
;; http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

(use-package notmuch-hello
  :defer t
  :config
  (setq notmuch-saved-searches
		'((:key "i" :name "inbox" :query "folder:INBOX")
		  (:key "b" :name "barebox" :query "folder:barebox")
		  (:key "c" :name "linux-can" :query "folder:linux-can")
		  (:key "a" :name "linux-arm-kernel" :query "folder:linux-arm-kernel")
		  (:key "m" :name "linux-mmc" :query "folder:linux-mmc")
		  (:key "9" :name "ath9k-devel" :query "folder:ath9k-devel")
		  (:key "e" :name "elecraft" :query "folder:elecraft")
		  (:key "p" :name "linuxham" :query "folder:linuxham")
		  (:key "p" :name "powertop" :query "folder:powertop")
		  (:key "n" :name "notmuch" :query "folder:notmuch")
		  (:key "D" :name "Deleted" :query "tag:deleted")
		  (:key "F" :name "Flagged" :query "tag:flagged")
		  (:key "S" :name "Sent" :query "folder:sent")
		  (:key "u" :name "unread" :query "tag:unread")
		  ))

  ;; We add items later in reverse order with (add-to-list ...):
  (setq notmuch-hello-sections '())

  ;; Add a thousand separator
  (setq notmuch-hello-thousands-separator ".")
  ;; don't display columns
  (setq notmuch-column-control 1.0)
;; This is GPLv3. If you still don't know the details, read
;; http://www.gnu.org/licenses/gpl-3.0.en.html

(defun my-notmuch-hello-insert-recent-searches ()
  "Insert recent searches."
  (when notmuch-search-history
    (widget-insert "Recent searches:")
    (widget-insert "\n\n")
    (let ((start (point)))
      (loop for i from 1 to notmuch-hello-recent-searches-max
	    for search in notmuch-search-history do
	    (let ((widget-symbol (intern (format "notmuch-hello-search-%d" i))))
	      (set widget-symbol
		   (widget-create 'editable-field
				  ;; Don't let the search boxes be
				  ;; less than 8 characters wide.
				  :size (max 8
					     (- (window-width)
						;; Leave some space
						;; at the start and
						;; end of the
						;; boxes.
						(* 2 notmuch-hello-indent)
						;; 1 for the space
						;; before the `[del]'
						;; button. 5 for the
						;; `[del]' button.
						1 5))
				  :action (lambda (widget &rest ignore)
					    (notmuch-hello-search (widget-value widget)))
				  search))
	      (widget-insert " ")
	      (widget-create 'push-button
			     :notify (lambda (widget &rest ignore)
				       (when (y-or-n-p "Are you sure you want to delete this search? ")
					 (notmuch-hello-delete-search-from-history widget)))
			     :notmuch-saved-search-widget widget-symbol
			     "del"))
	    (widget-insert "\n"))
      (indent-rigidly start (point) notmuch-hello-indent))
    nil))

  (add-to-list 'notmuch-hello-sections #'my-notmuch-hello-insert-recent-searches)
  (add-to-list 'notmuch-hello-sections #'notmuch-hello-insert-search)
  ;; This is GPLv2. If you still don't know the details, read
  ;; http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

  (defface my-notmuch-hello-header-face
	'((t :foreground "white"
		 :background "blue"
		 :weight bold))
	"Font for the header in `my-notmuch-hello-insert-searches`."
	:group 'notmuch-faces)
  ;; This is GPLv3. If you still don't know the details, read
  ;; http://www.gnu.org/licenses/gpl-3.0.en.html

  (defun my-count-query (query)
	(with-temp-buffer
	  (insert query "\n")
	  (unless (= (call-process-region (point-min) (point-max) notmuch-command
									  t t nil "count" "--batch") 0)
		(notmuch-logged-error "notmuch count --batch failed"
"Please check that the notmuch CLI is new enough to support `count
--batch'. In general we recommend running matching versions of
the CLI and emacs interface."))

	  (goto-char (point-min))
	  (let ((n (read (current-buffer))))
		(if (= n 0)
			nil
		  (notmuch-hello-nice-number n)))))
  ;; This is GPLv2. If you still don't know the details, read
  ;; http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

  (defun my-notmuch-hello-query-insert (cnt query elem)
	(if cnt
		(let* ((str (format "%s" cnt))
			   (widget-push-button-prefix "")
			   (widget-push-button-suffix "")
			   (oldest-first (case (plist-get elem :sort-order)
							   (newest-first nil)
							   (oldest-first t)
							   (otherwise notmuch-search-oldest-first))))
		  (widget-create 'push-button
						 :notify #'notmuch-hello-widget-search
						 :notmuch-search-terms query
						 :notmuch-search-oldest-first oldest-first
						 :notmuch-search-type 'tree
						 str)
		  (widget-insert (make-string (- 8 (length str)) ? )))
	  (widget-insert "        ")))
  ;; This is GPLv2. If you still don't know the details, read
  ;; http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

  (defun my-notmuch-hello-insert-searches ()
	"Insert the saved-searches section."
	(widget-insert (propertize "New     Total      Key  List\n" 'face 'my-notmuch-hello-header-face))
	(mapc (lambda (elem)
			(when elem
			  (let* ((q_tot (plist-get elem :query))
					 (q_new (concat q_tot " AND tag:unread"))
					 (n_tot (my-count-query q_tot))
					 (n_new (my-count-query q_new)))
				(my-notmuch-hello-query-insert n_new q_new elem)
				(my-notmuch-hello-query-insert n_tot q_tot elem)
				(widget-insert "   ")
				(widget-insert (plist-get elem :key))
				(widget-insert "    ")
				(widget-insert (plist-get elem :name))
				(widget-insert "\n")
			  ))
			)
		  notmuch-saved-searches))
  (defun my-notmuch-hello-reposition-after-refresh ()
	(goto-char (point-min))
	(forward-line 1))
  (add-hook 'notmuch-hello-refresh-hook #'my-notmuch-hello-reposition-after-refresh)

  (add-to-list 'notmuch-hello-sections #'my-notmuch-hello-insert-searches)

;; this is the end of use-package notmuch:
)
(use-package notmuch-lib
  :commands (notmuch-logged-error)
  :config
  (setq notmuch-search-oldest-first nil)

  ;; remove poll function
  (define-key notmuch-common-keymap "G" nil)
  ;; remove search function (we already have a search field)
  (define-key notmuch-common-keymap "s" nil)
)
(use-package notmuch-show
  :commands (notmuch-show-get-message-id notmuch-show-get-from notmuch-show-get-date notmuch-show-get-subject)
  :config
  (setq notmuch-show-relative-dates nil)

  ;; do not indent messages when showing a message
  (setq notmuch-show-indent-messages-width 0)
  ;; only show this one, selected message
  (setq notmuch-show-only-matching-messages t)

  ;; remove archiving commands
  (define-key notmuch-show-mode-map "x" 'my-notmuch-export-patch)
  (define-key notmuch-show-mode-map "X" nil)
  (define-key notmuch-show-mode-map "a" nil)
  (define-key notmuch-show-mode-map "A" nil)
  ; M-TAB is in use by the window manager
  (define-key notmuch-show-mode-map (kbd "M-TAB") nil)
)
(use-package notmuch-tag
  :defer t
  :config

  ;; change "unread" into an icon
  (add-to-list 'notmuch-tag-formats
			   '("unread"
				 (notmuch-tag-format-image-data tag (notmuch-tag-tag-icon))))

  ;; hide "attachment" and "signed" properties
  (add-to-list 'notmuch-tag-formats '("attachment" (propertize tag 'invisible t)))
  (add-to-list 'notmuch-tag-formats '("signed" (propertize tag 'invisible t)))
)
(use-package notmuch-tree
  :commands (notmuch-tree notmuch-tree-tag-thread notmuch-tree-refresh-view notmuch-search-from-tree-current-query)
  :config
  (setq notmuch-tree-result-format
		'(("date" . "%12s  ")
		  ("authors" . "%-23s")
		  ((("tree" . "%s")
			("subject" . "%s"))
		   . " %-54s ")
		  ;;("tags" . "(%s)")
		  ))

  ;; view selected messages in full window instead of split-pane
  (setq notmuch-tree-show-out t)

  ;; add unread tree command
  (defun my-notmuch-tree-tag-thread-read ()
	"Mark whole tree as read"
	(interactive)
	(notmuch-tree-tag-thread '("-unread"))
	(notmuch-tree-refresh-view))
  (bind-key "." 'my-notmuch-tree-tag-thread-read notmuch-tree-mode-map)

  ;; remove archiving commands
  (define-key notmuch-tree-mode-map "a" nil)
  (define-key notmuch-tree-mode-map "A" nil)
  ;; Use "Z" to switch back to non-tree view
  (define-key notmuch-tree-mode-map "S" nil)
  (define-key notmuch-tree-mode-map "Z" #'notmuch-search-from-tree-current-query)
  ;; M-TAB is in use by the window manager
  (define-key notmuch-tree-mode-map (kbd "M-TAB") nil)
  ;; "x" to close the tree and exit isn't needed, we have "q" for this
  (define-key notmuch-tree-mode-map "x" #'my-notmuch-export-patch)
)
(use-package notmuch-mua
  :defer t
  :config
  (setq notmuch-mua-cite-function #'message-cite-original-without-signature)
)
(defun my-notmuch-export-patch ()
  (interactive)
  (let* ((from (notmuch-show-get-from))
		 (date (notmuch-show-get-date))
		 (subject (notmuch-show-get-subject))
		 (id (notmuch-show-get-message-id))
		 (filename subject)
		 (patchnum))
	(when (string-match "\\[PATCH.+?0*\\([0-9]+\\)/[0-9]+\\]" filename)
	  (setq patchnum (string-to-number (match-string 1 filename))))
	(setq filename (replace-regexp-in-string "\\[PATCH.*\\]" "" filename))
	(setq filename (replace-regexp-in-string "\[^a-zA-Z0-9]" "-" filename))
	(setq filename (replace-regexp-in-string "\\-+" "-" filename))
	(setq filename (replace-regexp-in-string "^-" "" filename))
	(setq filename (replace-regexp-in-string "-$" "" filename))
	(when patchnum
	  (setq filename (concat (format "%04d" patchnum) "-" filename)))
	(setq filename (concat "/tmp/" filename ".patch"))
	(save-excursion
	  (let ((buf (generate-new-buffer (concat "*notmuch-export-patch-" id "*"))))
		(with-current-buffer buf
		  (insert (format "Subject: %s\n" subject))
		  (insert (format "From: %s\n" from))
		  (insert (format "Date: %s\n" date))
		  (insert (format "Message-Id: %s\n\n" (substring id 3)))
		  (let ((coding-system-for-read 'no-conversion))
			(call-process notmuch-command nil t nil "show" "--part:1" id))
		  (write-file filename))
		(kill-buffer buf)))))

(provide 'my-notmuch)
