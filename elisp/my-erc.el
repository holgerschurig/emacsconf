(use-package erc
  :if (not noninteractive)
  :commands (my-erc-window-setup my-erc-mode-setup
    erc-format-@nick erc-current-nick erc-cmd-NICK
	erc erc-compute-server erc-compute-nick erc-compute-full-name)
  :config
  ;; Log all traffic to *erc-protocol*. This is useful to find the
  ;; numbers that you can use in `erc-hide-list' or
  ;; `erc-track-exclude-types'.
  ;; (setq erc-debug-irc-protocol t)

  (setq erc-modules '(;; autoaway       ; Set away status automatically
					  autojoin          ; Join channels automatically
					  button            ; Buttonize URLs, nicknames, and other text
					  ;; capab-identify ; Mark unidentified users on servers supporting CAPAB
					  completion        ; Complete nicknames and commands
					  ;; dcc            ; Provide Direct Client-to-Client support
					  fill              ; Wrap long lines
					  ;; identd         ; Launch an identd server on port 8113
					  irccontrols       ; Highlight or remove IRC control characters
					  keep-place        ; Leave point above un-viewed text
					  list              ; List channels in a separate buffer
					  log               ; Save buffers in logs
					  ;; match          ; Highlight pals, fools, and other keywords
					  menu              ; Display a menu in ERC buffers
					  move-to-prompt    ; Move to the prompt when typing text
					  netsplit          ; Detect netsplit
					  ;; networks       ; Provide data about IRC networks
					  noncommands       ; Don’t display non-IRC commands after evaluation
					  ;; notify         ; Notify when the online status of certain users changes
					  ;; notifications  ; Send notifications on PRIVMSG or nickname mentions
					  ;; page           ; Process CTCP PAGE requests from IRC
					  readonly          ; Make displayed lines read-only
					  ;; replace        ; Replace text in messages
					  ring              ; Enable an input history
					  ;; scrolltobottom ; Scroll to the bottom of the buffer
					  services          ; Identify to Nickserv (IRC Services) automatically
					  ;; smiley         ; Convert smileys to pretty icons
					  ;; sound          ; Play sounds when you receive CTCP SOUND requests
					  spelling          ; Check spelling
					  stamp             ; Add timestamps to messages
					  track             ; Track channel activity in the mode-line
					  truncate          ; Truncate buffers to a certain size
					  ;; unmorse        ; Translate morse code in messages
					  ;; xdcc           ; Act as an XDCC file-server

					  ;; External Module
					  hl-nicks          ; nicely highlight the nicks
					  ))

  (setq erc-hide-list '("JOIN"
						"NICK"
						"PART"
						"QUIT"
						"MODE"
						;; See https://www.alien.net.au/irc/irc2numerics.html
						;;"353"		    ; NAMREPLY	RFC1459 (reply to NAMES)
						))

  (setq erc-server "irc.freenode.net"
		erc-nick "schurig"
		erc-prompt-for-password nil)

  ;; Kill buffers (we rely on erc-log to re-populate them)
  (setq erc-kill-queries-on-quit t
		erc-kill-server-buffer-on-quit t
		erc-kill-buffer-on-part t)

  ;; show all incoming CTCP messages
  (setq erc-paranoid t)

  ;; Show OPS as ops
  (setq erc-format-nick-function #'erc-format-@nick)

  ;; change the fill column
  (defun my-erc-window-setup ()
	(setq erc-fill-column (- (window-width) 2)))
  (add-hook 'window-configuration-change-hook #'my-erc-window-setup)
  (my-erc-window-setup)

  ;; private command /ID
  (defun erc-cmd-ID ()
    "Ghost old \"schurig\" and regain this id."
	(if (string= (erc-current-nick) "schurig")
		(message "You're already \"schurig\".")
	  (progn
		(erc-message "PRIVMSG" (format "nickserv ghost schurig %s" freenode-password))
		(erc-cmd-NICK "schurig")
		(erc-message "PRIVMSG" (format "nickserv identify schurig %s" freenode-password)))))
)


(use-package erc-backend
  :defer t
  :commands (erc-message)

  :custom
  ;; utf-8 always and forever
  (erc-server-coding-system '(utf-8 . utf-8))

  (erc-server-reconnect-attempts 5)
  (erc-server-reconnect-timeout 45)
)


(use-package erc-goodies
  :defer t
  :after erc
  :config
  ;; Interpret mIRC-style color commands in IRC chats
  (setq erc-interpret-mirc-color t)
  (setq erc-input-line-position -1)
)


(use-package erc-pcomplete
  :defer t
  :after erc
  :config
  (setq erc-pcomplete-nick-postfix ": ")
)


(use-package erc-join
  :defer t
  :after erc
  :config
  (setq erc-autojoin-channels-alist '(("freenode.net"
									   "#emacs"
									   "#md380"
									   "#iwd"
									   )
									  ("oftc.net"
									   "#awesome"
									   )))
  (setq erc-autojoin-timing 'ident)

  ;; Start ERC buffers in the background
  (setq erc-join-buffer 'bury)
)


(use-package erc-log
  :after erc
  :commands (my-erc-generate-log-file-name-with-date)
  :config
  ;; Set and create logging directory
  (setq erc-log-channels-directory (locate-user-emacs-file "tmp/erc-logs"))
  (unless (file-exists-p erc-log-channels-directory)
	  (mkdir erc-log-channels-directory t))

  ;; Simply file names, but with a date
  (defun my-erc-generate-log-file-name-with-date (buffer &rest ignore)
	"This function computes a short log file name.
The name of the log file is composed of BUFFER and the current date.
This function is a possible value for `erc-generate-log-file-name-function'.

This function differs from `erc-generate-log-file-name-with-date' by not exporting
the \"emacs\" instead of \"#emacs\"."
  (concat (substring (buffer-name buffer) 1) "-" (format-time-string "%Y-%m-%d") ".txt"))

  (setq erc-generate-log-file-name-function #'my-erc-generate-log-file-name-with-date)

  ;; Read log file into buffer upon connect
  (setq erc-log-insert-log-on-open t)

  ;; save immediately
  (setq erc-log-write-after-insert t
		erc-log-write-after-send t)
)


(use-package erc-services
  :defer t
  :after erc
  :config
  (setq erc-prompt-for-nickserv-password nil)
  (setq erc-nickserv-passwords
          `((freenode     (("schurig" . ,freenode-password)
                           ))
            ))
)


(use-package erc-spelling
  :after erc
  :commands (erc-spelling-mode)
  :config
  (erc-spelling-mode 1)

  ;; set different dictionaries by different servers/channels
  ;;(setq erc-spelling-dictionaries '(("#emacs" "american")))
)


(use-package erc-stamp
  :defer t
  :custom
  ;; I don't want spaces when several messages have the same timestamp
  (erc-timestamp-only-if-changed-flag nil)
  (erc-insert-timestamp-function #'erc-insert-timestamp-left)
  (erc-timestamp-format "%H:%M ")
  (erc-timestamp-intangible t)
)


(use-package erc-track
  :after erc
  :commands (erc-track-switch-buffer erc-track-mode)
  :config
  ;; exclude boring stuff from tracking
  (erc-track-mode t)
  (setq erc-track-exclude-types '("JOIN"
								  "NICK"
								  "PART"
								  "QUIT"
								  "MODE"
								  ;; See https://www.alien.net.au/irc/irc2numerics.html
								  "324" ; CHANNELMODEIS	RFC1459	<channel> <mode> <mode_params>
								  "329" ; CREATIONTIME
								  "332" ; TOPIC	RFC1459	<channel> :<topic>
								  "333" ; TOPICWHOTIME
								  "353" ; NAMREPLY	RFC1459 (reply to NAMES)
								  "477" ; NOCHANMODES	RFC2812	<channel> :<reason>
								  ))
  ;; shorten channel names
  (setq erc-track-shorten-start 3)
)


(use-package erc-truncate
  :after erc
  :commands (erc-truncate-mode)
  :config
  (erc-truncate-mode +1)
)


(use-package erc-hl-nicks
  :straight t
  :defer t
  :after erc
)


(defun freenode (arg)
  "Open Freenode IRC session.

If IRC isn't yet started, then start it. Otherwise switch to the
next buffer that has traffic."
  (interactive "P")
  (if (and (not arg) (get-buffer "chat.freenode.net:6667"))
	  (erc-track-switch-buffer 1)
	(message "Starting ERC ...")
	(erc :server "chat.freenode.net"
		 :port 6667
		 :nick "schurig"
		 :full-name user-full-name
		 )
	))



(defun oftc ()
  "Open OFTC IRC session.

If IRC isn't yet started, then start it. Otherwise switch to the
next buffer that has traffic."
  (interactive)
  (if (get-buffer "irc.oftc.net:6667")
	  (erc-track-switch-buffer 1)
	(message "Starting ERC ...")
	(erc :server "irc.oftc.net"
		 :port 6667
		 :nick "schurig"
		 :full-name user-full-name
		 )))

(provide 'my-erc)
