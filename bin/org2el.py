#!/usr/bin/python

import sys, re

debug = False

fname = "config.org"
if len(sys.argv) == 2:
	fname = sys.argv[1]
else:
	fname = "config.org"

f = open(fname)


reHEAD = re.compile(r'(\*+)\s+(.*)')


insrc = False
incmt = False
for s in f.readlines():
	s = s.rstrip()
	if debug:
		print "S:", s

	if s == "":
		if debug:
			print "EMPTY"
		else:
			print
		continue

	if insrc:
		if s.startswith("#+END_SRC"):
			if debug: print "INSRC_FALSE"
			insrc = False
			print
			continue
		print s
		continue

	if incmt:
		if s.startswith("# END_SRC"):
			incmt = False
			print
			if debug: print "INCMT_FALSE"
			continue
		print ";;", s
		continue

	if s.startswith("#+BEGIN_SRC emacs-lisp"):
		if debug: print "INSRC_TRUE"
		insrc = True
		continue

	if s.startswith("# BEGIN_SRC"):
		if debug: print "INCMT_TRUE"
		incmt = True
		continue

	# if s.startswith("#+"):
	# 	continue

	m = reHEAD.match(s)
	if m:
		n = len(m.group(1))
		if debug:
			print "HEAD", n
		else:
			print ";;%s %s" % (";"*n, m.group(2))
		continue

	print ";;", s
