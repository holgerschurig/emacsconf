#!/bin/bash


update_straight_repos()
{
	orig=`pwd`
	for i in straight/repos/${1}*/.git; do
		i="${i%/.git}"
		p=`basename $i`
		test "$p" = "elpa" && continue
		test "$p" = "melpa" && continue
		test "$p" = "epkgs" && continue
		echo -e "\n---> $p <---"
		(
			log="${orig}/tmp/log.$p"
			cd $i
			git pull
			git log --reverse --no-merges -p ORIG_HEAD..HEAD | tee --append ${log}
			test -s ${log} || rm -f ${log}
		)
	done
}

cd ~/.emacs.d

update_straight_repos

# Recompile notmuch when needed
test -f tmp/log.notmuch   && make -C straight/repos/notmuch

# Recompile pdf-tools when needed
test -f tmp/log.pdf-tools && make -C straight/build/pdf-tools/build/server
