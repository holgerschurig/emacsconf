;;; Editing
;;;; Package: undo-tree - treat undo history as a tree

;; This lets you use `C-z' (undo-tree-visualize) to visually walk
;; through the changes you've made, undo back to a certain point (or
;; redo), and go down different branches.

(use-package undo-tree
  :straight t
  :diminish undo-tree-mode
  :bind (("C-z" . undo-tree-visualize)
         :map undo-tree-map
         ("M-/" . undo-tree-redo))
  :commands (undo-tree-visualize global-undo-tree-mode)
  :config
  (global-undo-tree-mode 1)
  ;; Don't show Undo Tree in the mode line.
  (setq undo-tree-mode-lighter nil)
  ;; Disable undo-in-region. It sounds like an interesting feature,
  ;; but unfortunately the implementation is very buggy and regularly
  ;; causes you to lose your undo history.
  (setq undo-tree-enable-undo-in-region nil)

  (setq undo-tree-visualizer-timestamps t)
  (setq undo-tree-visualizer-diff t))



;;; Cursor movement

;; First we define code that allows us to bind multiple functions to
;; repeated commands. Taken from
;; http://www.emacswiki.org/cgi-bin/wiki/DoubleKeyBinding:

(defvar seq-times 0
  "Stores number of times command was executed.  It cotnains
random data before `seq-times' macro is called.")

(defmacro seq-times (&optional name max &rest body)
  "Returns number of times command NAME was executed and updates
`seq-times' variable accordingly.  If NAME is nil `this-command'
will be used.  If MAX is specified the counter will wrap around
at the value of MAX never reaching it.  If body is given it will
be evaluated if the command is run for the first time in a
sequence."
  (declare (indent 2))

  ;; Build incrementation part
  (setq max (cond ((null max) '(setq seq-times (1+ seq-times)))
		  ((atom max) (if (and (integerp max) (> max 0))
				  `(setq seq-times (% (1+ seq-times) ,max))
				'(setq seq-times (1+ seq-times))))
		  (t          `(let ((max ,max))
				 (if (and (integerp max) (> max 0))
					 (setq seq-times (% (1+ seq-times) max))
				   (setq seq-times (1+ seq-times)))))))

  ;; Make macro
  (if (eq name 'last-command)
	  max
	(cond ((null  name) (setq name 'this-command))
	  ((consp name) (setq name `(or ,name this-command))))
	`(if (eq last-command ,name)
	 ,max
	   ,@body
	   (setq seq-times 0))))

(defmacro seq-times-nth (name body &rest list)
  "Calls `seq-times' with arguments NAME, length and BODY
and (where length is the number of elements in LIST) then returns
`seq-times'th element of LIST."
  (declare (indent 2))
  `(nth (seq-times ,name ,(length list) ,body) ',list))

(defmacro seq-times-do (name body &rest commands)
  "Calls `seq-times' with arguments NAME, length and BODY (where
length is the number of COMMANDS) and then runs `seq-times'th
command from COMMANDS."
  (declare (indent 2))
  `(eval (nth (seq-times ,name ,(length commands) ,body) ',commands)))



;;;; Home / End

(defvar my--previous-position)

(defun my-home ()
  "Depending on how many times it was called moves the point to:

   - begin of indentation
   - beginning of line
   - begin of function
   - beginning of buffer
   - back to where it was"
  (interactive)
  (seq-times-do nil (setq my--previous-position (point))
	(back-to-indentation)
	(beginning-of-line)
	(beginning-of-defun)
	(goto-char (point-min))
	(goto-char my--previous-position)))

(bind-key "C-a" 'my-home)
(bind-key "<home>" 'my-home)

(defun my-end ()
  "Depending on how many times it was called moves the point to:

   - end of line
   - end of function
   - end of buffer
   - back to where it was"
  (interactive)
  (seq-times-do nil (setq my--previous-position (point))
	(end-of-line)
	(forward-paragraph)
	(end-of-defun)
	(goto-char (point-max))
	(goto-char my--previous-position)))

(bind-key "C-e" 'my-end)
(bind-key "<end>" 'my-end)



;;;; Recenter

(setq recenter-positions '(middle 4 -4))



;;;; Nicer goto-line

;; Doesn't modify minibuffer-history, but use it's own little history
;; list.

(defvar my-goto-line-history '())
(defun my-goto-line (line &optional buffer)
  "Goto LINE, counting from line 1 at beginning of buffer.
Normally, move point in the current buffer, and leave mark at the
previous position.  With just \\[universal-argument] as argument,
move point in the most recently selected other buffer, and switch to it.

If there's a number in the buffer at point, it is the default for LINE.

This function is usually the wrong thing to use in a Lisp program.
What you probably want instead is something like:
  (goto-char (point-min)) (forward-line (1- N))
If at all possible, an even better solution is to use char counts
rather than line counts."
  (interactive
   (if (and current-prefix-arg (not (consp current-prefix-arg)))
	   (list (prefix-numeric-value current-prefix-arg))
	 ;; Look for a default, a number in the buffer at point.
	 (let* ((default
		  (save-excursion
		(skip-chars-backward "0-9")
		(if (looking-at "[0-9]")
			(buffer-substring-no-properties
			 (point)
			 (progn (skip-chars-forward "0-9")
				(point))))))
		;; Decide if we're switching buffers.
		(buffer
		 (if (consp current-prefix-arg)
		 (other-buffer (current-buffer) t)))
		(buffer-prompt
		 (if buffer
		 (concat " in " (buffer-name buffer))
		   "")))
	   ;; Read the argument, offering that number (if any) as default.
	   (list (read-from-minibuffer (format (if default "Goto line%s (%s): "
						 "Goto line%s: ")
					   buffer-prompt
					   default)
				   nil nil t
				   'my-goto-line-history
				   default)
		 buffer))))
  ;; Switch to the desired buffer, one way or another.
  (if buffer
	  (let ((window (get-buffer-window buffer)))
	(if window (select-window window)
	  (switch-to-buffer-other-window buffer))))
  ;; Leave mark at previous position
  (or (region-active-p) (push-mark))
  ;; Move to the specified line number in that buffer.
  (save-restriction
	(widen)
	(goto-char (point-min))
	(if (eq selective-display t)
	(re-search-forward "[\n\C-m]" nil 'end (1- line))
	  (forward-line (1- line)))))

(bind-key "M-g g"   'my-goto-line)
(bind-key "M-g M-g" 'my-goto-line)



;;;; Mouse scrolling

;; Make the mouse wheel scroll smoother.

(setq mouse-wheel-scroll-amount '(2 ((shift) . 1) ((control) . nil))
	  mouse-wheel-progressive-speed nil)



;;;; Smooth scrolling

;; Normally, at the top-of-screen or bottom-of-screen, emacs would
;; scroll quarter-page wise. This is a bit annoying, because I'll
;; always have to chase the cursor when this happens. But there's a
;; cure:

(setq scroll-conservatively most-positive-fixnum
      scroll-preserve-screen-position t)



;;;; Builtin package: bookmark - set bookmarks, maybe annotate them, jump to them later

(use-package bookmark
  :if (not noninteractive)
  :config
  (setq bookmark-default-file (locate-user-emacs-file "tmp/bookmarks.el"))
)



;;;; Package: avy - jump to characters

(use-package avy
  :straight t
  :bind ("C-ö" . avy-goto-char-timer)

  :config
  (setq avy-keys (append (number-sequence ?a ?z)
						 (number-sequence ?0 ?9)))
  (setq avy-style 'at-full)
  (setq avy-all-windows nil)
  (setq avy-highlight-first t)
)



;;;; Package: expand-region - increase selected region by semantic units

;; Home page: https://github.com/magnars/expand-region.el
;;
;; C-+ Expand region increases the selected region by semantic units.
;;
;; You can then either continue to press C-+ to expand even further, or
;; use + and - after the first expand to expand further / shrink again.

(use-package expand-region
  :straight t
  :bind ("C-+" . er/expand-region)

  :config
  (setq expand-region-reset-fast-key    "<ESC><ESC>"))



;;;; Package: smartscan - jumps between other symbols found at point

;; This makes =M-n= and =M-p= look for the symbol at point. This is
;; very un-intrusive, no pop-up, no nothing,

(use-package smartscan
  :straight t
  :commands (global-smartscan-mode)

  :config
  (global-smartscan-mode t)
)



;;; Yank and Delete

;;;; Delete word or yank

;; The following may be of interest to people who (a) are happy with
;; "C-w" and friends for killing and yanking, (b) use
;; "transient-mark-mode", (c) also like the traditional Unix tty
;; behaviour that "C-w" deletes a word backwards. It tweaks "C-w" so
;; that, if the mark is inactive, it deletes a word backwards instead
;; of killing the region. Without that tweak, the C-w would create an
;; error text without an active region.
;; http://www.emacswiki.org/emacs/DefaultKillingAndYanking#toc2

(defadvice kill-region (before unix-werase activate compile)
  "When called interactively with no active region, delete a single word
	backwards instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
	 (list (save-excursion (backward-word 1) (point)) (point)))))



;;;; Selection deletion

;; Use delete-selection mode:

(delete-selection-mode t)



;;;; Deletion in readonly buffer

;; Be silent when killing text from read only buffer:

(setq kill-read-only-ok t)



;;;; Join lines at killing

;; If at end of line, join with following; otherwise kill line.
;; Deletes whitespace at join.

(defun kill-and-join-forward (&optional arg)
  "If at end of line, join with following; otherwise kill line.
Deletes whitespace at join."
  (interactive "P")
  (if (and (eolp) (not (bolp)))
	  (delete-indentation t)
	(kill-line arg)))

(bind-key "C-k" 'kill-and-join-forward)



;;;; X11 clipboard

(when (display-graphic-p)
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))



;;;; Mouse yank

;; Paste at text-cursor, not at mouse-cursor:

(setq mouse-yank-at-point t)


;;;; Package: avy-zap - delete up to a character

;; This makes =M-z= ask via avy to which character text should be
;; deleted. The character itself will stay. If you use =M-Z=, then this
;; character will be gone, too.

(use-package avy-zap
  :straight t
  :bind (("M-z" . avy-zap-up-to-char-dwim)
		 ("M-Z" . avy-zap-to-char-dwim))
)



;;; Searching

;;;; Builtin package: isearch - incremental search

(use-package isearch
  :defer t
  :bind (;; Allow to easily search for the last yanked string:
		 :map isearch-mode-map
		 ("C-y" . isearch-yank-kill))

  :config
  ;; Scrolling (including C-s) while searching:
  (setq isearch-allow-scroll t)

  ;; Do less flickering be removing highlighting immediately
  (setq lazy-highlight-initial-delay 0)
)



;;;; TODO Command: (my-grep)

;; Prompts you for an expression, defaulting to the symbol that your
;; cursor is on, and greps for that in the current directory and all
;; subdirectories:

;; (defun my-grep ()
;;   "grep the whole directory for something defaults to term at cursor position"
;;   (interactive)
;;   (let ((default (thing-at-point 'symbol)))
;; 	(let ((needle (or (read-string (concat "grep for '" default "': ")) default)))
;; 	  (setq needle (if (equal needle "") default needle))
;; 	  (grep (concat "egrep -s -i -n -r " needle " *")))))
;;
;; (bind-key "M-s g" 'my-grep)
;; this is now git-grep

;;; File opening/saving
;;;; Basic settings

;; Never show GTK file open dialog

(setq use-file-dialog nil)


;; Visit the real file by following symlinks. That's nice for editing config files
;; that are managed by =stow=.

(setq-default find-file-visit-truename t)


;; Don't create those pesky =.#foo= lockfiles:

(setq create-lockfiles nil)


;; Don't add newlines to end of buffer when scrolling, but show them

(setq next-line-add-newlines nil)


;; Preserve hard links to the file you´re editing (this is
;; especially important if you edit system files)

(setq backup-by-copying-when-linked t)


;; Just never create backup files at all
;; make-backup-files nil

(setq backup-directory-alist (list (cons "." (locate-user-emacs-file "tmp/bak/"))))


;; Make sure your text files end in a newline

(setq require-final-newline t)


;; Disable auto-save (#init.el# file-names)

(setq auto-save-default nil)
(setq auto-save-list-file-prefix (locate-user-emacs-file "tmp/auto-save-list/saves-"))


;; Kill means kill, not asking.

(setq kill-buffer-query-functions nil)


;;;; Automatically load .Xresources after changes

;; Sample ~/.Xresources:

;; Emacs.geometry: 120x55
;; Emacs.Font:	terminus 11

(defun merge-x-resources ()
  (let ((file (file-name-nondirectory (buffer-file-name))))
	(when (or (string= file ".Xdefaults")
		  (string= file ".Xresources"))
	  (start-process "xrdb" nil "xrdb" "-merge" (buffer-file-name))
	  (message (format "Merged %s into X resource database" file)))))
(add-hook 'after-save-hook 'merge-x-resources)


;;;; Decompress compressed files

(auto-compression-mode t)


;;;; Quickly save (F2)


(bind-key "<f2>" 'save-buffer)


;;;; Command: (sudo-edit)

;; From http://emacsredux.com/blog/2013/04/21/edit-files-as-root/

(defun sudo-edit (&optional arg)
  "Edit currently visited file as root.

With a prefix ARG prompt for a file to visit.
Will also prompt for a file to visit if current
buffer is not visiting a file."
  (interactive "P")
  (if (or arg (not buffer-file-name))
      (find-file (concat "/sudo:root@localhost:"
                         (ido-read-file-name "Find file(as root): ")))
    (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))


;;;; Builtin package: autorevert - reload changed files

;; Revert all buffers, including dired buffers. And do it silently.

(use-package autorevert
  :if (not noninteractive)
  :diminish auto-revert-mode
  :commands (global-auto-revert-mode)
  :init
  (global-auto-revert-mode 1)
  :config
  (setq global-auto-revert-non-file-buffers t
		auto-revert-interval 1
		revert-without-query '(".*")
		auto-revert-verbose nil)
)


;; Don't ask when running revert-buffer when reverting files in this
;; list of regular expressions:

(setq revert-without-query '(""))


;;;; Builtin package: recentf - store all edited file names

(use-package recentf
  :if (not noninteractive)
  :defer nil
  :commands (recentf-mode)
  :config
  (setq recentf-save-file (locate-user-emacs-file "tmp/recentf.el")
		recentf-exclude '("^/tmp/"
						  "/\\.newsrc"
						  ".*CMakeFiles.*"
						  "bbdb$"
						  "svn-commit\\.tmp$"
						  ".*-autoloads\\.el\\'"
						  "\\.png$"
						  "COMMIT_EDITMSG" "COMMIT_EDITMSG" "TAG_EDITMSG")
		recentf-max-saved-items 1000
		recentf-auto-cleanup 300
		recentf-max-menu-items 20)
  (recentf-mode 1)
)
