;;; Buffers

;;;; Insert buffer

(bind-key "C-x I" 'insert-buffer)



;;;; Protect buffers

;; Protect some buffers by only burying them instead of killing.
;;
;; https://raw.githubusercontent.com/lewang/le_emacs_libs/master/keep-buffers.el

(define-minor-mode keep-buffers-mode
  "when active, killing protected buffers results in burying them instead.
Some may also be erased, which is undo-able."
  :init-value nil
  :global t
  :group 'keep-buffers
  :lighter ""
  :version "1.4"
  (if keep-buffers-mode
	  ;; Setup the hook
	  (add-hook 'kill-buffer-query-functions 'keep-buffers-query)
	(remove-hook 'kill-buffer-query-functions 'keep-buffers-query)))

(defvar keep-buffers-protected-alist
  '(("\\`\\*scratch\\*\\'" . erase)
	("\\`\\*Messages\\*\\'" . nil))
  "an alist with either `(regex1 . 'erase)' or `(regex . nil)'.

CAR of each cons cell is the buffer matching regexp.  If CDR is
not nil then the matching buffer is erased then buried.

If the CDR is nil, then the buffer is only buried.")

(defun keep-buffers-query ()
  "The query function that disable deletion of buffers we protect."
  (let ((crit (dolist (crit keep-buffers-protected-alist)
				(when (string-match (car crit) (buffer-name))
				  (cl-return crit)))))
	(if crit
		(progn
		  (when (cdr crit)
			(erase-buffer))
		  (bury-buffer)
		  nil)
	  t)))

(keep-buffers-mode 1)



;;;; Easier kill buffers with processes

;; Don't asks you if you want to kill a buffer with a live process
;; attached to it:

(remove-hook 'kill-buffer-query-functions 'server-kill-buffer-query-function)



;;;; Buffers without toolbar, extra frame etc

(add-to-list 'special-display-buffer-names "*Backtrace*")
(add-to-list 'special-display-frame-alist '(tool-bar-lines . 0))



;;;; Package: iflipb - stack of recently edited files

;; http://www.emacswiki.org/emacs/iflipb

(use-package iflipb
  :straight t
  :commands (iflipb-next-buffer iflipb-previous-buffer)
  :bind ("S-<f6>" . my-iflipb-previous-buffer)
  :config
  (setq iflipb-wrap-around t)

  (defvar my-iflipb-auto-off-timeout-sec 4.5)
  (defvar my-iflipb-auto-off-timer-canceler-internal nil)
  (defvar my-iflipb-ing-internal nil)
  (defun my-iflipb-auto-off ()
	(message nil)
	(setq my-iflipb-auto-off-timer-canceler-internal nil
	  my-iflipb-ing-internal nil))
  (defun my-iflipb-next-buffer (arg)
	(interactive "P")
	(iflipb-next-buffer arg)
	(if my-iflipb-auto-off-timer-canceler-internal
	(cancel-timer my-iflipb-auto-off-timer-canceler-internal))
	(run-with-idle-timer my-iflipb-auto-off-timeout-sec 0 'my-iflipb-auto-off)
	(setq my-iflipb-ing-internal t))
  (defun my-iflipb-previous-buffer ()
	(interactive)
	(iflipb-previous-buffer)
	(if my-iflipb-auto-off-timer-canceler-internal
	(cancel-timer my-iflipb-auto-off-timer-canceler-internal))
	(run-with-idle-timer my-iflipb-auto-off-timeout-sec 0 'my-iflipb-auto-off)
	(setq my-iflipb-ing-internal t))
  (defun iflipb-first-iflipb-buffer-switch-command ()
	"Determines whether this is the first invocation of
  iflipb-next-buffer or iflipb-previous-buffer this round."
	(not (and (or (eq last-command 'my-iflipb-next-buffer)
		  (eq last-command 'my-iflipb-previous-buffer))
		  my-iflipb-ing-internal))))



;;;; Builtin package: unquify - unique buffer names

(use-package uniquify
  :if (not noninteractive)
  :config
  (setq uniquify-buffer-name-style 'forward)
)


;;; Frames
;;;; Frame title (currently unused)

;; Include current buffer name in the title bar
;; (setq frame-title-format '(buffer-file-name "%f" ("%b")))



;;; Window handling

;;;; Modify (delete-window)

;; If only one window in frame, `delete-frame'.
;;
;; From http://www.emacswiki.org/emacs/frame-cmds.el

(defadvice delete-window (around delete-window (&optional window) activate)
  (interactive)
  (save-current-buffer
	(setq window (or window (selected-window)))
	(select-window window)
	(if (one-window-p t)
	(delete-frame)
	  ad-do-it (selected-window))))



;;;; New (kill-buffer-and-window)

;; Replacement for interactive `kill-buffer'. We cannot redefine
;; `kill-buffer', because other elisp code relies on it's exact
;; behavior.

(defun my--kill-buffer-and-window (&optional buffer)
  "Kill buffer BUFFER-OR-NAME.
The argument may be a buffer or the name of an existing buffer.
Argument nil or omitted means kill the current buffer. Return t
if the buffer is actually killed, nil otherwise.

Unlike `kill-buffer', this also will delete the current window if
there are several windows open."
  (interactive)
  (setq buffer (or buffer (current-buffer)))
  (unless (one-window-p)
	(delete-window))
  (kill-buffer buffer))

(bind-key "C-x k" 'my--kill-buffer-and-window)



;;;; Window zooming (F5)

;; If there is only one window displayed, act like =C-x 2=. If there are
;; two windows displayed, act like =C-x 1=.

(defun my-zoom-next-buffer2 ()
  (let ((curbuf (current-buffer))
	(firstbuf nil))
	(dolist (buffer (buffer-list))
	  (with-current-buffer buffer
	;(princ (format "name %s, fn %s\n" (buffer-name) buffer-file-name))
	(unless (or
		 ;; Don't mention internal buffers.
		 (string= (substring (buffer-name) 0 1) " ")
		 ;; No buffers without files.
		 (not buffer-file-name)
		 ;; Skip the current buffer
		 (eq buffer curbuf)
		 )
	  ;(princ (format " nme %s, fn %s\n" (buffer-name) buffer-file-name))
	  (unless firstbuf
		(setq firstbuf buffer))
		;;(print buffer)
	  )))
	(when firstbuf
	  ;(princ (format "new buffer: %s.\n" firstbuf))
	  (bury-buffer)
	  (switch-to-buffer firstbuf))))

(defun my-explode-window ()
  "If there is only one window displayed, act like C-x2. If there
are two windows displayed, act like C-x1:"
  (interactive)
  (if (one-window-p t)
	  (progn
	(split-window-vertically)
	(other-window 1)
	(my-zoom-next-buffer2)
	(other-window -1))
	(delete-other-windows)))

(bind-key "<f5>" 'my-explode-window)



;;;; Windows toggle / Buffer switching (F6)

;; If there is only one window displayed, swap it with previous buffer.
;; If there are two windows displayed, act like =C-x o=.

;; See also Shift-F6 !

(defun my-switch-to-buffer ()
  "If there is only one window displayed, swap it with previous buffer.
If there are two windows displayed, act like =C-x o=."
  (interactive)
  (if (one-window-p t)
	  (switch-to-buffer (other-buffer (current-buffer) 1))
	(other-window -1)))

(bind-key "<f6>" 'my-switch-to-buffer)



;;;; TODO Open some buffers in the same window

;; (add-to-list 'same-window-buffer-names "*Messages*")
;; (add-to-list 'same-window-buffer-names "*shell*")


;;;; Builtin package: winner - restore old window configurations

(use-package winner
  :if (not noninteractive)
  :defer 10
  :commands (winner-mode)
  :init
  (winner-mode 1))

