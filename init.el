;; Don't load old .elc files when the .el file is newer
(setq load-prefer-newer t)


(let ((file-name-handler-alist nil)
      ;; effectively disable garbage collection during startup, default is 400000
      (gc-cons-threshold (* 512 1024 1024)))
  ;; (load (expand-file-name "trace-require" user-emacs-directory) t)
  (load (expand-file-name "config" user-emacs-directory))
  (message "Start up time %s" (emacs-init-time))
  )
