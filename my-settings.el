;;; Personal information

(setq user-full-name "Holger Schurig")
(setq user-mail-address "holgerschurig@gmail.com")


;; Passwords that shouldn't end up in my public git tree. Also not that I
;; make `freenode-password' known via `defvar', so that the
;; byte-compiler won't bark at me.

(defvar freenode-password nil "Password for the IRC network freenode.net")
(require 'private nil 'noerror)


;;; Browsing: browse-url - setup default browser

(use-package browse-url
  :commands (browse-url)

  :config
  (setq browse-url-browser-function 'browse-url-generic
		browse-url-generic-program "x-www-browser")
)



;;; Commands: Enable some disabled commands

(put 'erase-buffer 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'update-region 'disabled nil)



;;; Customization: cus-edit: set and load customization file

(use-package cus-edit
  :init
  (setq custom-file (locate-user-emacs-file "custom.el"))
  (load custom-file 'noerror 'nomessage)

  :config
  ;; keep lisp names in the custom buffers, don't capitalize.
  (setq custom-unlispify-tag-names nil)
  ;; kill old buffers.
  (setq custom-buffer-done-kill t)
)



;;; Edit window: Truncation

;; Don't display continuation lines
(setq-default truncate-lines t)

;; Do `M-x toggle-truncate-lines` to toggle truncation mode.
;; `truncate-partial-width-windows' has to be nil for
;; `toggle-truncate-lines' to work in split windows
(setq truncate-partial-width-windows nil)


;;; Edit window: Show trailing whitespace when programming

(defun my-show-trailing-whitespace ()
  (interactive)
  (setq show-trailing-whitespace t))
(defun my-hide-trailing-whitespace ()
  (interactive)
  (message "hide trailing whitespace")
  (setq show-trailing-whitespace nil))
(add-hook 'prog-mode-hook 'my-show-trailing-whitespace)


;;; Edit window: highlight keywords

;; See https://www.emacswiki.org/emacs/AddKeywords for the example and
;; https://www.emacswiki.org/emacs-test/RegularExpression for regular
;; expession description. Currently, I use:
;;
;; - \\< :: start of word
;; - \\( :: start of group
;; - \\) :: end of group
;; - \\| :: or
;; - \\? :: optional
;;
;; Add font locking for =FIXME=, =TODO=, =XXX= and =HINT= to all modes
;; except diff-mode and org-mode.

(defface my-todo-face
  '((t :foreground "red"
	   :weight bold))
  "Font for showing TODO words."
  :group 'basic-faces)

(defun my-add-font-lock-keywords ()
  (unless (or (eq 'diff-mode major-mode)
		  (eq 'org-mode major-mode))
	(font-lock-add-keywords nil
							'(("\\<\\(\\(FIXME\\|TODO\\|XXX\\|HINT\\):?\\)" 1 'font-lock-warning-face prepend)))))
(add-hook 'find-file-hook #'my-add-font-lock-keywords)


;; Mark =\todo= in LaTeX mode:
;; (font-lock-add-keywords 'latex-mode '(("\\(\\\\todo\\)" 1 'font-lock-warning-face prepend)))


;;; Edit window: sentence ending

;; A sentence doesn't end with two spaces:
(setq sentence-end-double-space nil)



;;; Edit window: font-lock - syntax highlighting

(use-package font-lock
  :if (not noninteractive)
  :config
  (setq font-lock-maximum-decoration 2)
  (setq-default font-lock-multiline t)
)


;;; Edit window: paren - let parenthesis behave

(use-package paren
  ;; TODO :if (not noninteractive)
  ;; TODO :defer nil
  :commands (show-paren-mode)
  :config
  (show-paren-mode 1)
  (setq show-paren-delay 0)
)


;;; Edit window: white-space - visualize tabs, (hard) spaces, newlines

;; The following can visualize white space quite neatly:

(use-package whitespace
  :defer t
  :bind ("C-c w" . whitespace-mode)
  ;; TODO :after zenburn
  :config
  (setq whitespace-style
	'(face
	  trailing
	  tabs
	  spaces
	  lines
	  lines-tail
	  newline
	  ;;empty
	  space-before-tab
	  indentation
	  empty
	  space-after-tab
	  space-mark
	  tab-mark
	  ;;newline-mark
	  ))
)

;;; GUI: Startup screen

(setq inhibit-startup-screen t)

;; Disabling greeting in the echo area is rather nasty, because a simple
;; setting of this variable by setq is deliberately ignored. Sigh.
(eval-after-load "startup" '(fset 'display-startup-echo-area-message 'ignore))

;;; GUI: Scratch message

;; Empty scratch message
(setq initial-scratch-message nil)

;;; GUI: Tool bar

(when (functionp 'tool-bar-mode)
  (tool-bar-mode -1))

;; GUI: Scroll bar

(unless (eq system-type 'windows-nt)
  (scroll-bar-mode -1))

;;; GUI: Blend fringe

;; http://emacs.stackexchange.com/a/5343/115

(set-face-attribute 'fringe nil
                    :foreground (face-foreground 'default)
                    :background (face-background 'default))


;;; GUI: simpler yes or no prompt

;  Get rid of yes-or-no questions - y or n is enough
(fset 'yes-or-no-p 'y-or-n-p)



;;; GUI: Settings for text vs. windowing systems

(if window-system
	;; X11, Windows, etc
	(progn
	  ;; Windowing systems are fast enought
	  (column-number-mode t)
	  ;; Turn off blinking
	  (blink-cursor-mode -1)
	  )
  ;; Text mode
  ;; No "very" visible cursor
  (setq visible-cursor nil))


;;; GUI: Mode line
;;;; Faces

(set-face-attribute 'mode-line           nil :background "light blue")
(set-face-attribute 'mode-line-buffer-id nil :background "blue" :foreground "white")

(defface mode-line-directory
  '((t :background "blue" :foreground "gray"))
  "Face used for buffer identification parts of the mode line."
  :group 'mode-line-faces
  :group 'basic-faces)

(set-face-attribute 'mode-line-highlight nil :box nil :background "deep sky blue")
(set-face-attribute 'mode-line-inactive  nil :inherit 'default)

;;;; Simplify the cursor position


(setq mode-line-position
			'(;; %p print percent of buffer above top of window, o Top, Bot or All
			  ;; (-3 "%p")
			  ;; %I print the size of the buffer, with kmG etc
			  ;; (size-indication-mode ("/" (-4 "%I")))
			  ;; " "
			  ;; %l print the current line number
			  ;; %c print the current column
			  (line-number-mode ("%l" (column-number-mode ":%c")))))

;;;; Directory shortening

(defun shorten-directory (dir max-length)
  "Show up to `max-length' characters of a directory name `dir'."
  (let ((path (reverse (split-string (abbreviate-file-name dir) "/")))
			   (output ""))
	   (when (and path (equal "" (car path)))
		 (setq path (cdr path)))
	   (while (and path (< (length output) (- max-length 4)))
		 (setq output (concat (car path) "/" output))
		 (setq path (cdr path)))
	   (when path
		 (setq output (concat ".../" output)))
	   output))

;;;; Directory name

(defvar mode-line-directory
  '(:propertize
	(:eval (if (buffer-file-name) (concat " " (shorten-directory default-directory 20)) " "))
				face mode-line-directory)
  "Formats the current directory.")
(put 'mode-line-directory 'risky-local-variable t)

(setq-default mode-line-buffer-identification
  (propertized-buffer-identification "%b "))


;;;; Binding it together

(setq-default mode-line-format
	  '("%e"
		mode-line-front-space
		;; mode-line-mule-info -- I'm always on utf-8
		mode-line-client
		mode-line-modified
		;; mode-line-remote -- no need to indicate this specially
		;; mode-line-frame-identification -- this is for text-mode emacs only
		" "
		mode-line-directory
		mode-line-buffer-identification
		" "
		mode-line-position
		;;(vc-mode vc-mode)  -- I use magit, not vc-mode
		(flycheck-mode flycheck-mode-line)
		" "
		mode-line-modes
		mode-line-misc-info
		mode-line-end-spaces))


;;; GUI: unhighlight if out of focus

;; I got the idea from here:
;; http://amitp.blogspot.de/2013/05/emacs-highlight-active-buffer.html


(defun highlight-focus:app-focus-in ()
  (global-font-lock-mode 1)
)
(defun highlight-focus:app-focus-out ()
  (global-font-lock-mode -1)
)
(add-hook 'focus-in-hook  #'highlight-focus:app-focus-in)
(add-hook 'focus-out-hook #'highlight-focus:app-focus-out)


;;; History: delete duplicates

;; Delete identical history entries

(setq history-delete-duplicates t)



;;; History: savehist - save mini-buffer history

(use-package savehist
  :defer nil
  :commands (savehist-mode)

  :init
  (defvar savehist-file)
  (setq savehist-file (locate-user-emacs-file "tmp/history.el")
		history-length 1000)

  :config
  (savehist-mode 1)
)



;;; Keys: no audible bell

(setq visible-bell t)


;;; Keys: let emacs react faster to keystrokes

(setq echo-keystrokes 0.1)
(setq idle-update-delay 0.35)


;;; Keys: keyfreq - record how often you use a keyboard command

;; Idea from http://blog.binchen.org/posts/how-to-be-extremely-efficient-in-emacs.html
;;
;; Use `keyfreq-show' to see what you've used mostly.

(use-package keyfreq
  :straight t
  :if (not noninteractive)
  :defer nil
  :commands (keyfreq-mode keyfreq-autosave-mode)
  :config
  (defun my-turnon-keyfreq-mode ()
	(interactive)
	(keyfreq-mode 1)
	(keyfreq-autosave-mode 1))

  (defun turnoff-keyfreq-mode ()
	(interactive)
	(keyfreq-mode -1)
	(keyfreq-autosave-mode -1))

  (add-to-list 'same-window-buffer-names "*frequencies*")

  (setq keyfreq-excluded-commands
		'(self-insert-command
		  org-self-insert-command
		  abort-recursive-edit
		  backward-char
		  delete-backward-char
		  forward-char
		  keyfreq-mode
		  previous-line
		  next-line
		  undefined ;; lambda
		  ))

  (setq keyfreq-file (locate-user-emacs-file "tmp/keyfreq.el"))

  (unless (file-exists-p (file-truename keyfreq-file))
	(with-temp-buffer
	  (insert "()")
	  (write-file (file-truename keyfreq-file))))

  ;; And use keyfreq-show to see how many times you used a command.
  ;; comment out below line if there is performance impact
  (my-turnon-keyfreq-mode)
)



;;; Messages: limit lines

(setq message-log-max 10000)



;;; Mouse: hide while editing

;; As soon as you type a character (i.E. moving the cursor isn't enough)
;; the mouse will be hidden.

(setq make-pointer-invisible t)



;;; Server: server

;; - always start the emacs-server, except when run in daemon mode
;;
;; - already Disable prompt asking you if you want to kill a buffer
;;   with a live process attached to it.
;;   http://stackoverflow.com/questions/268088/how-to-remove-the-prompt-for-killing-emacsclient-buffers

(use-package server
  :if (not noninteractive)
  :defer nil
  :commands (server-running-p server-mode server-edit)
  :config
  (unless (or (daemonp) (server-running-p))
	(server-mode 1))
  (add-hook 'server-switch-hook 'raise-frame)
)

;; A good way to start emacsclient is with this line in =/etc/bash.bashrc=:
;; alias e="emacsclient --no-wait --alternate-editor=\"\" --create-frame"

;; May may also set the environment variables =EDITOR= and/or =VISUAL=,
;; but then you better omit the "=--no-wait=" option.



